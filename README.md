# Netify Agent Core Processor Plugin
Copyright &copy; 2024 eGloo Incorporated

[![pipeline status](https://gitlab.com/netify.ai/public/netify-plugins/netify-proc-core/badges/master/pipeline.svg)](https://gitlab.com/netify.ai/public/netify-plugins/netify-proc-core/-/commits/master)

## Overview

The core processor plugin encodes flow events and Agent status updates into various output formats such as JSON or MSGPACK with optional compression support.  The encoded results can then be dispatched to one or more sink plugins for transport or storage.

## Documentation

Documentation can be found [here](doc/Netify%20Core%20Processor.md).

