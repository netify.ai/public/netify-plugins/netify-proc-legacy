// Netify Agent Core Processor
// Copyright (C) 2021-2024 eGloo Incorporated
// <http://www.egloo.ca>

#pragma once

#include <nd-flow-parser.hpp>
#include <nd-plugin.hpp>

#define _NPP_LEGACY_JSON_VERSION 1.9

class nppChannelConfig
{
public:
    enum class Type : uint8_t {
        NONE = 0,
        LEGACY_HTTP = (1 << 0),
        LEGACY_SOCKET = (1 << 1),
        STREAM_FLOWS = (1 << 2),
        STREAM_STATS = (1 << 3),
    };

    enum Format : uint8_t {
        RAW,
        JSON,
        MSGPACK,
    };

    enum Compressor : uint8_t {
        NONE,
        GZ,
    };

    nppChannelConfig()
      : format(Format::JSON), compressor(Compressor::NONE) { }

    void Load(const std::string &channel,
      const nlohmann::json &jconf);
    inline void Load(const std::string &channel,
      const nlohmann::json &jconf, nppChannelConfig &defaults) {
        types = defaults.types;
        format = defaults.format;
        compressor = defaults.compressor;
        Load(channel, jconf);
    }

    std::string channel;
    ndFlags<Type> types = { Type::NONE };
    Format format = { Format::RAW };
    Compressor compressor = { Compressor::NONE };
};

class nppFlowEvent
{
public:
    nppFlowEvent(ndPluginProcessor::Event event, ndFlow::Ptr &flow)
      : flow(flow), event(event), stats(flow->stats) { }

    ndFlow::Ptr flow;
    ndPluginProcessor::Event event;
    ndFlowStats stats;
};

class nppPlugin : public ndPluginProcessor
{
public:
    nppPlugin(const std::string &tag, const ndPlugin::Params &params);
    virtual ~nppPlugin();

    virtual void *Entry(void);

    virtual void GetVersion(std::string &version);

    virtual void GetStatus(nlohmann::json &status);
    virtual void DisplayStatus(const nlohmann::json &status);

    virtual void DispatchEvent(ndPlugin::Event event,
      void *param = nullptr);

    virtual void DispatchProcessorEvent(
      ndPluginProcessor::Event event, ndFlowMap *flow_map);
    virtual void DispatchProcessorEvent(
      ndPluginProcessor::Event event, ndFlow::Ptr &flow);
    virtual void DispatchProcessorEvent(
      ndPluginProcessor::Event event, ndInterfaces *interfaces);
    virtual void DispatchProcessorEvent(ndPluginProcessor::Event event,
      const std::string &iface, ndPacketStats *stats);
    virtual void DispatchProcessorEvent(
      ndPluginProcessor::Event event, ndPacketStats *stats);
    virtual void DispatchProcessorEvent(
      ndPluginProcessor::Event event, ndInstanceStatus *status);
    virtual void
    DispatchProcessorEvent(ndPluginProcessor::Event event);

protected:
    std::atomic<bool> reload;
    std::atomic<bool> dispatch_update;

    nlohmann::json status;
    std::mutex status_mutex;

    void Reload(void);

    pthread_cond_t lock_cond;
    pthread_mutex_t cond_mutex;

    nppChannelConfig defaults;
    std::map<std::string, std::map<std::string, nppChannelConfig>> sinks;

    std::atomic<ndFlags<nppChannelConfig::Type>> chan_types = {
        nppChannelConfig::Type::NONE
    };

    std::vector<nppFlowEvent> flow_events;
    std::vector<nppFlowEvent> flow_events_priv;

    ndFlowParser flow_parser;
    typedef std::vector<std::string> FlowFilters;
    FlowFilters flow_filters;

    virtual void DispatchPayload(nppChannelConfig::Type chan_type,
      const nlohmann::json &jpayload);

    void EncodeFlow(const nppFlowEvent &event,
      nlohmann::json &jpayload);

    nlohmann::json jagent_status;
    std::map<std::string, std::vector<nlohmann::json>> jflows;
    nlohmann::json jifaces;
    nlohmann::json jiface_endpoints;
    nlohmann::json jiface_stats;
    nlohmann::json jiface_packet_stats;

    void EncodeAgentStatus(ndInstanceStatus *status);
    void EncodeInterfaces(ndInterfaces *interfaces);
    void EncodeInterfaceStats(const std::string &iface,
      ndPacketStats *stats);
    void EncodeGlobalPacketStats(ndPacketStats *stats);

    void DispatchLegacyPayload(void);
    void DispatchStreamPayload(void);
};
