// Netify Agent Core Processor
// Copyright (C) 2021-2024 eGloo Incorporated
// <http://www.egloo.ca>
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General
// Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <fstream>

#include <nd-except.hpp>
#include <nd-util.hpp>

#include "npp-plugin.hpp"

using namespace std;
using json = nlohmann::json;

void nppChannelConfig::Load(const string &channel, const json &jconf) {
    auto it = jconf.find("types");
    if (it != jconf.end() && it->type() == json::value_t::array)
    {
        vector<string> types = it->get<vector<string>>();
        for (auto &type : types) {
            if (type == "legacy-http")
                this->types |= nppChannelConfig::Type::LEGACY_HTTP;
            else if (type == "legacy-socket")
                this->types |= nppChannelConfig::Type::LEGACY_SOCKET;
            else if (type == "stream-flows")
                this->types |= nppChannelConfig::Type::STREAM_FLOWS;
            else if (type == "stream-stats")
                this->types |= nppChannelConfig::Type::STREAM_STATS;
            else {
                throw ndException("%s: %s: %s: %s", __PRETTY_FUNCTION__,
                  "type", type.c_str(), strerror(EINVAL));
            }
        }
    }

    it = jconf.find("format");
    if (it != jconf.end() && it->type() == json::value_t::string)
    {
        string format = it->get<string>();
        if (format == "json")
            this->format = nppChannelConfig::Format::JSON;
        else if (format == "msgpack")
            this->format = nppChannelConfig::Format::MSGPACK;
        else {
            throw ndException("%s: %s: %s: %s", __PRETTY_FUNCTION__,
              "format", format.c_str(), strerror(EINVAL));
        }
    }

    it = jconf.find("compressor");
    if (it != jconf.end() && it->type() == json::value_t::string)
    {
        string compressor = it->get<string>();
        if (compressor == "none")
            this->compressor = nppChannelConfig::Compressor::NONE;
        else if (compressor == "gz")
            this->compressor = nppChannelConfig::Compressor::GZ;
        else {
            throw ndException("%s: %s: %s: %s",
              __PRETTY_FUNCTION__, "compressor",
              compressor.c_str(), strerror(EINVAL));
        }
    }
}

nppPlugin::nppPlugin(const string &tag, const ndPlugin::Params &params)
  : ndPluginProcessor(tag, params) {
    if (conf_filename.empty()) {
        throw ndException("%s: %s: %s", __PRETTY_FUNCTION__,
          "conf_filename", strerror(EINVAL));
    }

    reload = true;
    dispatch_update = false;

    int rc;
    pthread_condattr_t cond_attr;

    pthread_condattr_init(&cond_attr);
    pthread_condattr_setclock(&cond_attr, CLOCK_MONOTONIC);
    if ((rc = pthread_cond_init(&lock_cond, &cond_attr)) != 0)
    {
        throw ndExceptionSystemErrno(tag.c_str(),
          "pthread_cond_init", rc);
    }

    pthread_condattr_destroy(&cond_attr);

    if ((rc = pthread_mutex_init(&cond_mutex, nullptr)) != 0)
    {
        throw ndExceptionSystemErrno(tag.c_str(),
          "pthread_mutex_init", rc);
    }

    nd_dprintf("%s: initialized\n", tag.c_str());
}

nppPlugin::~nppPlugin() {
    pthread_cond_broadcast(&lock_cond);

    Join();

    pthread_cond_destroy(&lock_cond);
    pthread_mutex_destroy(&cond_mutex);

    nd_dprintf("%s: destroyed\n", tag.c_str());
}

void *nppPlugin::Entry(void) {
    int rc;

    nd_printf(
      "%s: %s v%s Copyright (C) 2021-2024 eGloo "
      "Incorporated.\n",
      tag.c_str(), PACKAGE_NAME, PACKAGE_VERSION);

    for (;;) {
        if (reload.load()) {
            Reload();
            reload = false;
        }

        Lock();

        if (flow_events.empty()) {
            Unlock();

            if (flow_events_priv.empty() && ShouldTerminate())
                break;

            if ((rc = pthread_mutex_lock(&cond_mutex)) != 0) {
                throw ndExceptionSystemErrno(tag.c_str(),
                  "pthread_mutex_lock", rc);
            }

            struct timespec ts_cond;
            if (clock_gettime(CLOCK_MONOTONIC, &ts_cond) != 0)
            {
                throw ndExceptionSystemError(tag.c_str(),
                  "clock_gettime");
            }

            ts_cond.tv_sec += 1;
            if ((rc = pthread_cond_timedwait(&lock_cond,
                   &cond_mutex, &ts_cond)) != 0 &&
              rc != ETIMEDOUT)
            {
                throw ndExceptionSystemErrno(tag.c_str(),
                  "pthread_cond_timedwait", rc);
            }

            if ((rc = pthread_mutex_unlock(&cond_mutex)) != 0)
            {
                throw ndExceptionSystemErrno(tag.c_str(),
                  "pthread_mutex_unlock", rc);
            }

            continue;
        }

        while (! flow_events.empty()) {
            flow_events_priv.push_back(flow_events.back());
            flow_events.pop_back();
        }

        Unlock();

        if (dispatch_update.load()) {
            dispatch_update = false;

            if (ndFlagBooleanAny(chan_types.load(),
                  nppChannelConfig::Type::LEGACY_HTTP |
                    nppChannelConfig::Type::LEGACY_SOCKET))
                DispatchLegacyPayload();

            if (ndFlagBoolean(chan_types.load(),
                  nppChannelConfig::Type::STREAM_STATS))
                DispatchStreamPayload();

            jagent_status.clear();
            jflows.clear();
            jifaces.clear();
            jiface_endpoints.clear();
            jiface_stats.clear();
            jiface_packet_stats.clear();
        }

        while (! flow_events_priv.empty()) {
            if (! flow_filters.empty()) {
                bool match = false;
                for (auto &expr : flow_filters) {
                    try {
                        if ((match = flow_parser.Parse(
                               flow_events_priv.back().flow, expr)))
                            break;
                    }
                    catch (string &e) {
                        nd_dprintf("%s: %s: %s\n", tag.c_str(),
                          expr.c_str(), e.c_str());
                    }
                }

                if (! match) {
                    flow_events_priv.pop_back();
                    continue;
                }
            }

            json jpayload;
            EncodeFlow(flow_events_priv.back(), jpayload);
            flow_events_priv.pop_back();

            if (jpayload.empty() == false) {
                DispatchPayload(nppChannelConfig::Type::LEGACY_SOCKET,
                  jpayload);
                DispatchPayload(nppChannelConfig::Type::STREAM_FLOWS,
                  jpayload);
            }
        }
    }

    return nullptr;
}

void nppPlugin::GetVersion(string &version) {
    version = PACKAGE_VERSION;
}

void nppPlugin::GetStatus(nlohmann::json &status) {
#if 0
    lock_guard<mutex> lg(status_mutex);
    status["aggregator"] = aggregator;
    status["samples"] = samples.size();
#endif
}

void nppPlugin::DisplayStatus(const nlohmann::json &status) {
#if 0
    unsigned a = 0, s = 0;

    auto i = status.find("license_status_id");
    if (i != status.end() && i->is_number_unsigned())
        nlm.DisplayLicenseStatus(i->get<unsigned>());

    i = status.find("aggregator");
    if (i != status.end() && i->is_number_unsigned())
        a = i->get<unsigned>();

    i = status.find("samples");
    if (i != status.end() && i->is_number_unsigned())
        s = i->get<unsigned>();

    fprintf(stdout, "%s aggregator #%u\n", ndTerm::Icon::INFO, a);
    fprintf(stdout, "%s samples: %u\n", ndTerm::Icon::INFO, s);
#endif
}

void nppPlugin::DispatchEvent(ndPlugin::Event event, void *param) {
    switch (event) {
    case ndPlugin::Event::RELOAD: reload = true; break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(
  ndPluginProcessor::Event event, ndFlowMap *flow_map) {
    switch (event) {
    case ndPluginProcessor::Event::FLOW_MAP: break;
    default: return;
    }

    if (! ndFlagBooleanAny(chan_types.load(),
          nppChannelConfig::Type::LEGACY_HTTP |
            nppChannelConfig::Type::LEGACY_SOCKET |
            nppChannelConfig::Type::STREAM_STATS))
        return;

    Lock();

    size_t buckets = flow_map->GetBuckets();

    for (size_t b = 0; b < buckets; b++) {
        auto &fm = flow_map->Acquire(b);

        for (auto &it : fm) {
            if (! it.second->flags.detection_complete.load())
                continue;
            if (it.second->flags.expired.load()) continue;
            if (it.second->stats.lower_packets.load() == 0 &&
              it.second->stats.upper_packets.load() == 0)
                continue;

            flow_events.push_back(nppFlowEvent(event, it.second));
        }

        flow_map->Release(b);
    }

    bool broadcast = (! flow_events.empty());

    Unlock();

    if (broadcast) {
        int rc;
        if ((rc = pthread_cond_broadcast(&lock_cond)) != 0) {
            throw ndExceptionSystemErrno(tag.c_str(),
              "pthread_cond_broadcast", rc);
        }
    }
}

void nppPlugin::DispatchProcessorEvent(
  ndPluginProcessor::Event event, ndFlow::Ptr &flow) {
    switch (event) {
    case ndPluginProcessor::Event::DPI_NEW:
    case ndPluginProcessor::Event::DPI_UPDATE:
    case ndPluginProcessor::Event::DPI_COMPLETE:
    case ndPluginProcessor::Event::FLOW_EXPIRE: break;
    default: return;
    }

    if (! ndFlagBooleanAny(chan_types.load(),
          nppChannelConfig::Type::LEGACY_SOCKET |
            nppChannelConfig::Type::STREAM_FLOWS))
        return;

    Lock();

    flow_events.push_back(nppFlowEvent(event, flow));

    Unlock();

    int rc;
    if ((rc = pthread_cond_broadcast(&lock_cond)) != 0) {
        throw ndExceptionSystemErrno(tag.c_str(),
          "pthread_cond_broadcast", rc);
    }
}

void nppPlugin::DispatchProcessorEvent(
  ndPluginProcessor::Event event, ndInterfaces *interfaces) {
    switch (event) {
    case ndPluginProcessor::Event::INTERFACES:
        EncodeInterfaces(interfaces);
        break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(ndPluginProcessor::Event event,
  const string &iface, ndPacketStats *stats) {
    switch (event) {
    case ndPluginProcessor::Event::PKT_CAPTURE_STATS:
        EncodeInterfaceStats(iface, stats);
        break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(
  ndPluginProcessor::Event event, ndPacketStats *stats) {
    switch (event) {
    case ndPluginProcessor::Event::PKT_GLOBAL_STATS:
        EncodeGlobalPacketStats(stats);
        break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(
  ndPluginProcessor::Event event, ndInstanceStatus *status) {
    switch (event) {
    case ndPluginProcessor::Event::UPDATE_INIT:
        EncodeAgentStatus(status);
        break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(ndPluginProcessor::Event event) {
    switch (event) {
    case ndPluginProcessor::Event::UPDATE_COMPLETE:
        dispatch_update = true;
        break;
    default: break;
    }
}

void nppPlugin::Reload(void) {
    nd_dprintf("%s: Loading configuration: %s\n",
      tag.c_str(), conf_filename.c_str());

    json j;
    ifstream ifs(conf_filename);
    if (! ifs.is_open()) {
        throw ndExceptionSystemErrno(tag.c_str(),
          conf_filename.c_str(), ENOENT);
    }

    try {
        ifs >> j;
    }
    catch (json::exception &e) {
        throw ndException("%s: %s: JSON exception: %s",
          tag.c_str(), conf_filename.c_str(), e.what());
    }
    catch (exception &e) {
        throw ndException("%s: %s: %s", tag.c_str(),
          conf_filename.c_str(), e.what());
    }

    defaults.Load("defaults", j);

    chan_types = defaults.types;

    Lock();

    sinks.clear();

    try {
        auto jflow_filters = j.find("flow_filters");
        if (jflow_filters != j.end() &&
          jflow_filters->type() == json::value_t::array)
        {
            flow_filters = jflow_filters->get<FlowFilters>();
        }

        auto jsinks = j.find("sinks");

        if (jsinks != j.end()) {
            for (auto &jsink : jsinks->get<json::object_t>())
            {
                for (auto &jchannel :
                  jsink.second.get<json::object_t>())
                {
                    auto it = jchannel.second.find(
                      "enable");
                    if (it != jchannel.second.end() &&
                      it->type() == json::value_t::boolean &&
                      it->get<bool>() != true)
                        continue;

                    nppChannelConfig config;

                    config.Load(jchannel.first,
                      jchannel.second, defaults);

                    chan_types = (chan_types.load() | config.types);

                    auto sink = sinks.find(jsink.first);
                    if (sink == sinks.end()) {
                        map<string, nppChannelConfig> entry;

                        entry.insert(
                          make_pair(jchannel.first, config));
                        sinks.insert(make_pair(jsink.first, entry));
                    }
                    else {
                        sink->second.insert(
                          make_pair(jchannel.first, config));
                    }
                }
            }
        }
    }
    catch (exception &e) {
        Unlock();
        throw e;
    }

    Unlock();
}

void nppPlugin::DispatchPayload(
  nppChannelConfig::Type chan_type, const json &jpayload) {
    for (auto &sink : sinks) {
        for (auto &channel : sink.second) {
            if (! ndFlagBoolean(channel.second.types, chan_type))
                continue;

            ndFlags<ndPlugin::DispatchFlags> flags =
              (chan_type == nppChannelConfig::Type::LEGACY_SOCKET) ?
              (ndPlugin::DispatchFlags::ADD_CR |
                ndPlugin::DispatchFlags::ADD_HEADER) :
              ndPlugin::DispatchFlags::NONE;

            switch (channel.second.format) {
            case nppChannelConfig::Format::JSON:
                flags |= ndPlugin::DispatchFlags::FORMAT_JSON;
                break;
            case nppChannelConfig::Format::MSGPACK:
                flags |= ndPlugin::DispatchFlags::FORMAT_MSGPACK;
                break;
            default: break;
            }

            switch (channel.second.compressor) {
            case nppChannelConfig::Compressor::GZ:
                flags |= ndPlugin::DispatchFlags::GZ_DEFLATE;
                break;
            default: break;
            }

            ndPlugin::DispatchSinkPayload(sink.first,
              { channel.first }, jpayload, flags);
        }
    }
}

void nppPlugin::EncodeFlow(const nppFlowEvent &event, json &jpayload) {
    json jflow;
    ndFlags<ndFlow::EncodeFlags> encode_options =
      ndFlow::EncodeFlags::METADATA;

    switch (event.event) {
    case ndPluginProcessor::Event::DPI_COMPLETE:
    case ndPluginProcessor::Event::DPI_UPDATE:
    case ndPluginProcessor::Event::FLOW_MAP:
        encode_options |= ndFlow::EncodeFlags::STATS;
    case ndPluginProcessor::Event::DPI_NEW:
        encode_options |= ndFlow::EncodeFlags::TUNNELS;
        break;
    case ndPluginProcessor::Event::FLOW_EXPIRE:
        encode_options = ndFlow::EncodeFlags::STATS;
        break;
    default: return;
    }

    event.flow->Encode(jflow, event.stats, encode_options);

    if (event.event == ndPluginProcessor::Event::FLOW_MAP) {
        auto it = jflows.find(event.flow->iface->ifname);
        if (it != jflows.end()) it->second.push_back(jflow);
        else {
            vector<json> jf = { jflow };
            jflows.insert(make_pair(event.flow->iface->ifname, jf));
        }
    }

    switch (event.event) {
    case ndPluginProcessor::Event::DPI_NEW:
        jpayload["type"] = "flow";
        break;
    case ndPluginProcessor::Event::DPI_UPDATE:
        jpayload["type"] = "flow_dpi_update";
        break;
    case ndPluginProcessor::Event::DPI_COMPLETE:
        jpayload["type"] = "flow_dpi_complete";
        break;
    case ndPluginProcessor::Event::FLOW_MAP:
        jpayload["type"] = "flow_stats";
        jflow.clear();
        event.flow->Encode(jflow, event.stats,
          ndFlow::EncodeFlags::STATS);
        break;
    case ndPluginProcessor::Event::FLOW_EXPIRE:
        jpayload["type"] = "flow_purge";
        jpayload["reason"] =
          (event.flow->ip_protocol == IPPROTO_TCP &&
            event.flow->tcp.fin_ack.load()) ?
          "closed" :
          "expired";
        break;
    default: return;
    }

    jpayload["interface"] = event.flow->iface->ifname;
    jpayload["internal"] = (event.flow->iface->role ==
      ndInterfaceRole::LAN);
    // XXX: Deprecated
    // jpayload["established"] = false;
    jpayload["flow"] = jflow;
}

void nppPlugin::EncodeAgentStatus(ndInstanceStatus *status) {
    status->Encode(jagent_status);
}

void nppPlugin::EncodeInterfaces(ndInterfaces *interfaces) {
    static const vector<string> keys = { "addr" };

    for (auto &i : *interfaces) {
        json jo;
        i.second->Encode(jo);
        i.second->EncodeAddrs(jo, keys);

        jifaces[i.second->ifname] = jo;

        i.second->EncodeEndpoints(
          i.second->LastEndpointSnapshot(), jiface_endpoints);
    }
}

void nppPlugin::EncodeInterfaceStats(const string &iface,
  ndPacketStats *stats) {
    json jo;
    stats->Encode(jo);

    jiface_stats[iface] = jo;
}

void nppPlugin::EncodeGlobalPacketStats(ndPacketStats *stats) {
    json jo;
    stats->Encode(jo);

    jiface_packet_stats = jo;
}

void nppPlugin::DispatchLegacyPayload(void) {
    json jpayload(jagent_status);
    jpayload["version"] = _NPP_LEGACY_JSON_VERSION;
    jpayload["flows"] = jflows;
    jpayload["interfaces"] = jifaces;
    jpayload["devices"] = jiface_endpoints;
    jpayload["stats"] = jiface_stats;

    DispatchPayload(nppChannelConfig::Type::LEGACY_HTTP, jpayload);

    jpayload.clear();
    jpayload["type"] = "agent_hello";
    jpayload["agent_version"] = nd_get_version();
    jpayload["build_version"] = nd_get_version_and_features();
    jpayload["json_version"] = _NPP_LEGACY_JSON_VERSION;

    DispatchPayload(nppChannelConfig::Type::LEGACY_SOCKET, jpayload);

    jpayload.clear();
    jpayload = jagent_status;
    jpayload["type"] = "agent_status";

    DispatchPayload(nppChannelConfig::Type::LEGACY_SOCKET, jpayload);
}

void nppPlugin::DispatchStreamPayload(void) {
    json jpayload(jagent_status);

    jpayload["type"] = "agent_status";
    jpayload["agent_version"] = nd_get_version();
    jpayload["build_version"] = nd_get_version_and_features();

    DispatchPayload(nppChannelConfig::Type::STREAM_STATS, jpayload);

    jpayload.clear();
    jpayload = jifaces;
    jpayload["type"] = "interfaces";

    DispatchPayload(nppChannelConfig::Type::STREAM_STATS, jpayload);

    jpayload.clear();
    jpayload = jiface_endpoints;
    jpayload["type"] = "endpoints";

    DispatchPayload(nppChannelConfig::Type::STREAM_STATS, jpayload);

    jpayload.clear();
    jpayload = jiface_stats;
    jpayload["type"] = "interface_stats";

    DispatchPayload(nppChannelConfig::Type::STREAM_STATS, jpayload);

    jpayload.clear();
    jpayload = jiface_packet_stats;
    jpayload["type"] = "global_stats";

    DispatchPayload(nppChannelConfig::Type::STREAM_STATS, jpayload);
}

ndPluginInit(nppPlugin);
